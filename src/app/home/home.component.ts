import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss', '../../styles/utility.scss']
})
export class HomeComponent implements OnInit {
  private onDestroy = new Subject();

  constructor() { }

  ngOnInit() {
  }
}
