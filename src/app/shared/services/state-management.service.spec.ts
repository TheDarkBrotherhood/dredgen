import { TestBed } from '@angular/core/testing';

import { StateManagementService } from './state-management.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StateManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: StateManagementService = TestBed.get(StateManagementService);
    expect(service).toBeTruthy();
  });
});
