import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BungieApiService } from './bungie-api.service';
import { HistoricalStatsResponse } from '../models/bungie-api/HistoricalStats';
import { DestinyProfile } from '../models/bungie-api/DestinyProfile';

describe('BungieApiService', () => {
  let bungieApiService: BungieApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BungieApiService]
    });

    bungieApiService = TestBed.get(BungieApiService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => httpMock.verify());

  it('should recieve a destiny player', (done) => {
    const playerSearch = require('../../../mock-data/SearchDestinyPlayerMock.json');

    bungieApiService.searchDestinyPlayer('-1', 'Theo Bassaw').subscribe(player => {
      expect(player.Response[0].displayName).toEqual('Theo Bassaw');
    });

    const req = httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/SearchDestinyPlayer/-1/Theo Bassaw/');
    expect(req.request.method).toBe('GET');
    req.flush(playerSearch);

    done();
  });

  it('should recieve a destiny player profile', (done) => {
    const destinyProfile = require('../../../mock-data/getDestinyProfileMock.json');

    bungieApiService.getProfile('4611686018450491554', '1', {components: '100'}).subscribe((profile: DestinyProfile) => {
      expect(profile.Response.profile.data.userInfo.displayName).toEqual('Theo Bassaw');
    });

    const req =
      httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/1/Profile/4611686018450491554/?components=100');
    expect(req.request.method).toBe('GET');
    req.flush(destinyProfile);

    done();
  });

  it('should recieve a destiny player historical stats', (done) => {
    const destinyStats = require('../../../mock-data/HistoricalStats.json');

    bungieApiService.getHistoricalStats('4611686018450491554', '1', '0', {modes: '64'}).subscribe((stats: HistoricalStatsResponse) => {

    });

    const req =
      httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/1/Account/4611686018450491554/Character/0/Stats/?modes=64');
    expect(req.request.method).toBe('GET');
    req.flush(destinyStats);

    done();
  });
});
