import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateManagementService {
  key: string;

  constructor(private http: HttpClient) { }

  getKeyOnStartUp(): Promise<any> {
    return this.http
      .get('../../assets/settings.json')
      .toPromise()
      .then((settings: any) => {this.key = settings.apiKey; return settings; })
      .catch((err: any) => Promise.resolve());
  }

  getApiKey(): string {
    return this.key;
  }
}
