import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SearchDestinyPlayerResponse } from '../models/bungie-api/SearchDestinyPlayer';
import { StateManagementService } from './state-management.service';


@Injectable({
  providedIn: 'root'
})
/*
  Service that handles all requests to the Bungie Api that don't require oauth.
*/
export class BungieApiService {
  baseUrl = 'https://www.bungie.net/Platform/';

  constructor(private http: HttpClient, private state: StateManagementService) { }

  /*
    Returns all Destiny Accounts based on a supplied platform type and username.
  */
  searchDestinyPlayer(membershipType: string, displayName: string): Observable<SearchDestinyPlayerResponse> {
    let header = new HttpHeaders().set('X-API-Key', this.state.key);
    return this.http.get<SearchDestinyPlayerResponse>(this.baseUrl + 'Destiny2/SearchDestinyPlayer/' + membershipType + '/' + displayName
      + '/', {headers: header});
  }

  /*
    Returns all data of a user's profile based on a supplied membershipType, membershipId,
    and query params.
  */
  getProfile(destinyMembershipId: string, membershipType: string, query): Observable<any> {
    let header = new HttpHeaders().set('X-API-Key', this.state.key);
    return this.http.get(this.baseUrl + 'Destiny2/' + membershipType + '/Profile/' + destinyMembershipId
      + '/', {headers: header, params: query});
  }

  /*
    Returns all data of a user's stats based on a supplied membershipType, membershipId,
    characterId, and query params.
  */
  getHistoricalStats(destinyMembershipId: string, membershipType: string, characterId: string, query): Observable<any> {
    let header = new HttpHeaders().set('X-API-Key', this.state.key);
    return this.http.get(this.baseUrl + 'Destiny2/' + membershipType + '/Account/' + destinyMembershipId +
      '/Character/' + characterId + '/Stats/', {headers: header, params: query});
  }
}
