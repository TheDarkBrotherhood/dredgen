import { TestBed } from '@angular/core/testing';

import { ManifestService } from './manifest.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StateManagementService } from './state-management.service';

describe('ManifestService', () => {
  let manifestService: ManifestService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ManifestService, StateManagementService]
    });
    manifestService = TestBed.get(ManifestService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => httpMock.verify());

  it('should be created', () => {
    expect(manifestService).toBeTruthy();
  });

  it('should get manifest information', (done) => {
    const manifestEndpoint = require('../../../mock-data/DestinyManifestEndpointMock.json');

    manifestService.getManifestInfo().subscribe(info => {
        expect(info).toBeTruthy();
      }
    );

    const req = httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/Manifest/');
    expect(req.request.method).toBe('GET');
    req.flush(manifestEndpoint);

    done();
  });
});
