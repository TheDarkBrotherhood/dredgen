import { Injectable } from '@angular/core';
import { destinyManifest } from '../utility/dexie';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { BungieManifest } from 'src/app/shared/models/bungie-api/BungieManfest';
import { StateManagementService } from './state-management.service';
import { Observable, of } from 'rxjs';
import { Manifest } from '../models/Manifest';

@Injectable({
  providedIn: 'root'
})
/*
  Service that handles everything related to the manifest. Also can't mock the whole manifest so a lot of
  methods could not be tested.
*/
export class ManifestService {
  db = destinyManifest;

  constructor(private http: HttpClient, private state: StateManagementService) { }

  /*
    Simply gets the information provide from this endpoint such as manifest version and manifest urls.
  */
  getManifestInfo() {
    let header = new HttpHeaders().set('X-API-Key', this.state.getApiKey());
    return this.http.get<BungieManifest>('https://www.bungie.net/Platform/Destiny2/Manifest/', {headers: header});
  }
  /*
    Simply gets the entire Destiny Manifest. Can't test without mocking manifest.
  */
  getDestinyManifest(): Observable<Manifest> {
    return this.getManifestInfo().pipe(
      switchMap(manifestInfo => this.http.get('https://www.bungie.net/' +
       manifestInfo.Response.jsonWorldContentPaths.en).pipe(
        map(manifestData => {
          return {id: manifestInfo.Response.version, manifest: manifestData};
        })
      ))
    );
  }

  /*
    Downloads Manifest and adds it to indexeddb. Mainly if someone deletes the manifest or
    is new to website. Method only used with checkManifest(). Can't test without mocking manifest.
  */
  private downloadManifestIfNoneExist(): Observable<string> {
    return this.getDestinyManifest().pipe(
      map(manifest => {
        this.db.manifest.add(manifest);
        return 'Done adding manifest.';
      })
    );
  }

  /*
    Removes outdated Manifest and downloads the new Manifest and adds it to indexeddb.
    Method only used with checkManifest(). Can't test without mocking manifest.
  */
  private updateManifest(): Observable<string> {
    this.db.manifest.clear();
    return this.getDestinyManifest().pipe(
      map(manifest => {
        this.db.manifest.add(manifest);
        return 'Finished clearing and updated manifest';
      })
    );
  }

  /*
    Checks manifest version and does its respectful operation.
    Method only used with checkManifest(). Can't test without mocking manifest.
  */
  private checkManifestVersion(arr: Manifest[]): Observable<Observable<string>> {
    return this.getManifestInfo().pipe(
      map(manifestInfo => {
        if (arr[arr.length - 1].id.match(manifestInfo.Response.version)) { // If versions match
          return of('Manifest version is Up to date.');
        } else {
          return this.updateManifest(); // Updates manifest if version do not match.
        }
      })
    );
  }

  /*
    Checks manifest and then follows the chain of commands based on it.
    Method only used with checkManifest(). Can't test without mocking manifest.
  */
  checkManifest() {
    return this.db.manifest.toArray()
    .then(arr => {
      if (arr.length === 0) {// If no manifest exists mainly if someone deletes it or is new to website.
        return this.downloadManifestIfNoneExist();
      } else {
        return this.checkManifestVersion(arr);
      }
    })
    .catch(err => console.log(err));
  }

  /*
    Get Gambit Rank from manifest. Can't test without mocking manifest.
  */
  getGambitRank(gambitRankStep: number) {
    let gambitRankIcon: string;
    let gambitRank: string;
    return this.db.manifest.toArray()
    .then((manifest) => {
      gambitRankIcon = 'https://bungie.net/' +
        manifest[0].manifest['DestinyProgressionDefinition']['2772425241']['steps'][gambitRankStep]['icon'];
      gambitRank =
        manifest[0].manifest['DestinyProgressionDefinition']['2772425241']['steps'][gambitRankStep]['stepName'];
      return {icon: gambitRankIcon, rank: gambitRank};
    });
  }
}

