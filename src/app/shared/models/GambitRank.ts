export interface GambitRank {
    icon: string;
    rank: string;
}
