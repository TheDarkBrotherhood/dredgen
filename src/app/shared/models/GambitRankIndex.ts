export interface GambitRankIndex {
    stepIndex: number;
    resets: number;
}
