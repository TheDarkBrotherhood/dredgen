export interface UserInfo {
    membershipType: number;
    membershipId: string;
    displayName: string;
}

export interface Data {
    userInfo: UserInfo;
    dateLastPlayed: Date;
    versionsOwned: number;
    characterIds: string[];
}

export interface Profile {
    data: Data;
    privacy: number;
}

export interface ProfileResponse {
    profile: Profile;
}

export interface DestinyProfile {
    Response: ProfileResponse;
    ErrorCode: number;
    ThrottleSeconds: number;
    ErrorStatus: string;
    Message: string;
    MessageData: {};
}
