export interface BungieManifest {
    Response?: Response;
    ErrorCode?: number;
    ThrottleSeconds?: number;
    ErrorStatus?: string;
    Message?: string;
    MessageData?: {};
}

export interface Response {
    version?: string;
    mobileAssetContentPath?: string;
    mobileGearAssetDataBases?: MobileGearAssetDataBasis[];
    mobileWorldContentPaths?: WorldContentPaths;
    jsonWorldContentPaths?: WorldContentPaths;
    mobileClanBannerDatabasePath?: string;
    mobileGearCDN?: MobileGearCDN;
    iconImagePyramidInfo?: any[];
}

export interface WorldContentPaths {
    en?: string;
    fr?: string;
    es?: string;
    'es-mx'?: string;
    de?: string;
    it?: string;
    ja?: string;
    'pt-br'?: string;
    ru?: string;
    pl?: string;
    ko?: string;
    'zh-cht'?: string;
    'zh-chs'?: string;
}

export interface MobileGearAssetDataBasis {
    version?: number;
    path?: string;
}

export interface MobileGearCDN {
    Geometry?: string;
    Texture?: string;
    PlateRegion?: string;
    Gear?: string;
    Shader?: string;
}
