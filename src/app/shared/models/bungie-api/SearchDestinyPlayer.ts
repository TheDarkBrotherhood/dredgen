export interface DestinyPlayerResponse {
    iconPath: string;
    membershipType: number;
    membershipId: string;
    displayName: string;
}

export interface SearchDestinyPlayerResponse {
    Response: DestinyPlayerResponse[];
    ErrorCode: number;
    ThrottleSeconds: number;
    ErrorStatus: string;
    Message: string;
    MessageData: {};
}
