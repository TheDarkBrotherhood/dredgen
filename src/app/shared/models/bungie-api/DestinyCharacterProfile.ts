export interface DestinyCharacterProfile {
    Response: Response;
    ErrorCode: number;
    ThrottleSeconds: number;
    ErrorStatus: string;
    Message: string;
    MessageData: {};
}

export interface Response {
    characters: Characters;
}

export interface Characters {
    data: { [key: string]: Datum };
    privacy: number;
}

export interface Datum {
    membershipId: string;
    membershipType: number;
    characterId: string;
    dateLastPlayed: Date;
    minutesPlayedThisSession: string;
    minutesPlayedTotal: string;
    light: number;
    stats: { [key: string]: number };
    raceHash: number;
    genderHash: number;
    classHash: number;
    raceType: number;
    classType: number;
    genderType: number;
    emblemPath: string;
    emblemBackgroundPath: string;
    emblemHash: number;
    emblemColor: EmblemColor;
    levelProgression: LevelProgression;
    baseCharacterLevel: number;
    percentToNextLevel: number;
}

export interface EmblemColor {
    red: number;
    green: number;
    blue: number;
    alpha: number;
}

export interface LevelProgression {
    progressionHash: number;
    dailyProgress: number;
    dailyLimit: number;
    weeklyProgress: number;
    weeklyLimit: number;
    currentProgress: number;
    level: number;
    levelCap: number;
    stepIndex: number;
    progressToNextLevel: number;
    nextLevelAt: number;
}
