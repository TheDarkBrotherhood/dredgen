import { StatData, HistoricalStats } from './bungie-api/HistoricalStats';

export interface DetailedStats {
    historical: HistoricalStats;
    weapons: Weapons;
    gambit: Gambit;
}

export interface Gambit {
    gambit: GambitStats;
    prime: GambitStats;
}

export interface GambitStats {
    invasions?: StatData;
    invasionKills?: StatData;
    invasionDeaths?: StatData;
    invaderKills?: StatData;
    invaderDeaths?: StatData;
    primevalKills?: StatData;
    blockerKills?: StatData;
    mobKills?: StatData;
    highValueKills?: StatData;
    motesPickedUp?: StatData;
    motesDeposited?: StatData;
    motesDenied?: StatData;
    motesLost?: StatData;
    bankOverage?: StatData;
    smallBlockersSent?: StatData;
    mediumBlockersSent?: StatData;
    largeBlockersSent?: StatData;
    primevalDamage?: StatData;
    primevalHealing?: StatData;
}

export interface Weapons {
    gambit: WeaponStats;
    prime: WeaponStats;
    reckoning: WeaponStats;
}

export interface WeaponStats {
    weaponKills?: WeaponKillStats;
    precisonKills?: PrecisionWeaponKillStats;
    accuracy?: WeaponAccuracy;
}

export interface WeaponKillStats {
    weaponKillsAutoRifle?: StatData;
    weaponKillsBeamRifle?: StatData;
    weaponKillsBow?: StatData;
    weaponKillsFusionRifle?: StatData;
    weaponKillsHandCannon?: StatData;
    weaponKillsTraceRifle?: StatData;
    weaponKillsMachineGun?: StatData;
    weaponKillsPulseRifle?: StatData;
    weaponKillsRocketLauncher?: StatData;
    weaponKillsScoutRifle?: StatData;
    weaponKillsShotgun?: StatData;
    weaponKillsSniper?: StatData;
    weaponKillsSubmachinegun?: StatData;
    weaponKillsRelic?: StatData;
    weaponKillsSideArm?: StatData;
    weaponKillsSword?: StatData;
    weaponKillsAbility?: StatData;
    weaponKillsGrenade?: StatData;
    weaponKillsGrenadeLauncher?: StatData;
    weaponKillsSuper?: StatData;
    weaponKillsMelee?: StatData;
}

export interface PrecisionWeaponKillStats {
    weaponPrecisionKillsAutoRifle?: StatData;
    weaponPrecisionKillsBeamRifle?: StatData;
    weaponPrecisionKillsBow?: StatData;
    weaponPrecisionKillsFusionRifle?: StatData;
    weaponPrecisionKillsGrenade?: StatData;
    weaponPrecisionKillsHandCannon?: StatData;
    weaponPrecisionKillsMelee?: StatData;
    weaponPrecisionKillsPulseRifle?: StatData;
    weaponPrecisionKillsRocketLauncher?: StatData;
    weaponPrecisionKillsScoutRifle?: StatData;
    weaponPrecisionKillsShotgun?: StatData;
    weaponPrecisionKillsSniper?: StatData;
    weaponPrecisionKillsSubmachinegun?: StatData;
    weaponPrecisionKillsSuper?: StatData;
    weaponPrecisionKillsRelic?: StatData;
    weaponPrecisionKillsSideArm?: StatData;
}

export interface WeaponAccuracy {
    weaponKillsPrecisionKillsAutoRifle?: StatData;
    weaponKillsPrecisionKillsBeamRifle?: StatData;
    weaponKillsPrecisionKillsBow?: StatData;
    weaponKillsPrecisionKillsFusionRifle?: StatData;
    weaponKillsPrecisionKillsGrenade?: StatData;
    weaponKillsPrecisionKillsGrenadeLauncher?: StatData;
    weaponKillsPrecisionKillsHandCannon?: StatData;
    weaponKillsPrecisionKillsTraceRifle?: StatData;
    weaponKillsPrecisionKillsMachineGun?: StatData;
    weaponKillsPrecisionKillsMelee?: StatData;
    weaponKillsPrecisionKillsPulseRifle?: StatData;
    weaponKillsPrecisionKillsRocketLauncher?: StatData;
    weaponKillsPrecisionKillsScoutRifle?: StatData;
    weaponKillsPrecisionKillsShotgun?: StatData;
    weaponKillsPrecisionKillsSniper?: StatData;
    weaponKillsPrecisionKillsSubmachinegun?: StatData;
    weaponKillsPrecisionKillsSuper?: StatData;
    weaponKillsPrecisionKillsRelic?: StatData;
    weaponKillsPrecisionKillsSideArm?: StatData;
}

export interface MedalStats {
    medals_pvecomp_medal_invader_kill_four?: StatData;
    medals_pvecomp_medal_massacre?: StatData;
    medals_pvecomp_medal_no_escape?: StatData;
    medals_pvecomp_medal_blockbuster?: StatData;
    medals_pvecomp_medal_tags_denied_15?: StatData;
    medals_pvecomp_medal_bank_kill?: StatData;
    medals_pvecomp_medal_denied?: StatData;
    medals_pvecomp_medal_invasion_shutdown?: StatData;
    medals_pvecomp_medal_locksmith?: StatData;
    medals_pvecomp_medal_block_party?: StatData;
    medals_pvecomp_medal_never_say_die?: StatData;
    medals_pvecomp_medal_half_banked?: StatData;
    medals_pvecomp_medal_everyone_invaded?: StatData;
    medals_pvecomp_medal_killmonger?: StatData;
    medals_pvecomp_medal_thrillmonger?: StatData;
    medals_pvecomp_medal_overkillmonger?: StatData;
    medals_pvecomp_medal_first_to_block?: StatData;
    medals_pvecomp_medal_fast_fill?: StatData;
    medals_pvecomp_medal_big_game_hunter?: StatData;
    medals_pvecomp_medal_kill_after_invasion?: StatData;
    medals_pvecomp_medal_revenge?: StatData;
    medals_pvecomp_medal_revenge_same_invasion?: StatData;
}
