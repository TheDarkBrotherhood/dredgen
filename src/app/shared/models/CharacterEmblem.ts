export interface CharacterEmblem {
    characterId: string;
    displayName: string;
    lightLevel: number;
    level: number;
    emblemPath: string;
    classType: number;
    genderType: number;
}
