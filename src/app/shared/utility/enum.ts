export enum ActivityModes {
    Gambit = 63,
    AllPvECompetitive = 64,
    GambitPrime = 75,
    Reckoning = 76
}

export enum Activity {
    'Gambit' = 0,
    'PvE Competitive' = 1,
    'Gambit Prime' = 2,
    'Reckoning' = 3
}

export enum StatGroups {
    General = '1',
    Weapons = '2',
    Medals = '3'
}

export enum RaceType {
    Human = 0,
    Awoken = 1,
    Exo = 2,
    Unknown = 3
}

export enum ClassType {
    Titan = 0,
    Hunter = 1,
    Warlock = 2,
    Unknown = 3
}
