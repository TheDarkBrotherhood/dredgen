import Dexie from 'dexie';
import { Manifest } from '../models/Manifest';

class Database extends Dexie {
    manifest: Dexie.Table<Manifest, number>;

    constructor(databaseName) {
        super(databaseName);
        this.version(1).stores({
            destinyManifest:
            'id, destinyProgressionDefinition, destinyRaceDefinition, destinyClassDefinition, destinyHistoricalStatsDefinition'
        });

        this.version(2).stores({
            manifest: 'id, manifest'
        });
    }
}

export let destinyManifest = new Database('destinyManifest');
