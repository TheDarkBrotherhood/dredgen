import { TestBed } from '@angular/core/testing';

import { OverviewStatsResolverService } from './overview-stats-resolver.service';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

describe('GeneralStatsResolverService', () => {
  let bungieApiService: BungieApiService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BungieApiService]
    });
    bungieApiService = TestBed.get(BungieApiService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: OverviewStatsResolverService = TestBed.get(OverviewStatsResolverService);
    expect(service).toBeTruthy();
  });
});
