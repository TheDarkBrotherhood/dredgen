import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { map } from 'rxjs/operators';
import { HistoricalStats, Stats } from '../../shared/models/bungie-api/HistoricalStats';
import { ActivityModes, StatGroups } from '../../shared/utility/enum';
import {
  WeaponKillStats,
  MedalStats,
  PrecisionWeaponKillStats,
  WeaponAccuracy, WeaponStats, Weapons, GambitStats, Gambit, DetailedStats } from '../../shared/models/DredgenStats';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailedStatsResolverService implements Resolve<DetailedStats> {

  constructor(private service: BungieApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let membershipType = route.parent.paramMap.get('membershipType');
    let membershipId = route.parent.paramMap.get('membershipId');
    let characterId = route.paramMap.get('characterId');

    return this.getHistoricalStats(membershipId, membershipType, characterId);
  }

  getHistoricalStats(membershipId, membershipType, characterId): Observable<DetailedStats> {
    return this.service.getHistoricalStats(membershipId, membershipType, characterId, {
      modes: ActivityModes.Gambit + ',' + ActivityModes.GambitPrime + ',' + ActivityModes.Reckoning ,
      groups: StatGroups.General + ',' + StatGroups.Weapons + ',' + StatGroups.Medals
    }).pipe(
      map((historicalStats: HistoricalStats) => {
        let gambitWeaponStats: WeaponStats;
        let gambitPrimeWeaponStats: WeaponStats;
        let reckoningWeaponStats: WeaponStats;
        let weaponStats: Weapons;
        let gambitStats: GambitStats;
        let primeStats: GambitStats;
        let gambit: Gambit;
        if (historicalStats.Response.pvecomp_gambit.allTime != null) {
          gambitWeaponStats = {
            weaponKills: this.getWeaponKillStats(historicalStats.Response.pvecomp_gambit.allTime),
            precisonKills: this.getPrecisionKillStats(historicalStats.Response.pvecomp_gambit.allTime),
            accuracy: this.getWeaponAccuracy(historicalStats.Response.pvecomp_gambit.allTime),
          };
          gambitStats = this.getGambitStats(historicalStats.Response.pvecomp_gambit.allTime);
        } else {
          gambitWeaponStats = null;
          gambitStats = null;
        }
        if (historicalStats.Response.pvecomp_mamba.allTime != null) {
          gambitPrimeWeaponStats = {
            weaponKills: this.getWeaponKillStats(historicalStats.Response.pvecomp_mamba.allTime),
            precisonKills: this.getPrecisionKillStats(historicalStats.Response.pvecomp_mamba.allTime),
            accuracy: this.getWeaponAccuracy(historicalStats.Response.pvecomp_mamba.allTime),
          };
          primeStats = this.getGambitStats(historicalStats.Response.pvecomp_mamba.allTime);
        } else {
          gambitPrimeWeaponStats = null;
          primeStats = null;
        }
        if (historicalStats.Response.enigma.allTime != null) {
          reckoningWeaponStats = {
            weaponKills: this.getWeaponKillStats(historicalStats.Response.enigma.allTime),
            precisonKills: this.getPrecisionKillStats(historicalStats.Response.enigma.allTime),
            accuracy: this.getWeaponAccuracy(historicalStats.Response.enigma.allTime)
          };
        } else {
          reckoningWeaponStats = null;
        }
        weaponStats = {
          gambit: gambitWeaponStats,
          prime: gambitPrimeWeaponStats,
          reckoning: reckoningWeaponStats
        };
        gambit = {
          gambit: gambitStats,
          prime: primeStats
        };
        return {historical: historicalStats, weapons: weaponStats, gambit};
      }
    ));
  }

  getWeaponKillStats(stats: Stats): WeaponKillStats {
    let weaponKills: WeaponKillStats = {
      weaponKillsAutoRifle: stats.weaponKillsAutoRifle,
      weaponKillsBeamRifle: stats.weaponKillsBeamRifle,
      weaponKillsBow: stats.weaponKillsBow,
      weaponKillsFusionRifle: stats.weaponKillsFusionRifle,
      weaponKillsHandCannon: stats.weaponKillsHandCannon,
      weaponKillsTraceRifle: stats.weaponKillsTraceRifle,
      weaponKillsMachineGun: stats.weaponKillsMachineGun,
      weaponKillsPulseRifle: stats.weaponKillsPulseRifle,
      weaponKillsRocketLauncher: stats.weaponKillsRocketLauncher,
      weaponKillsScoutRifle: stats.weaponKillsScoutRifle,
      weaponKillsShotgun: stats.weaponKillsShotgun,
      weaponKillsSniper: stats.weaponKillsSniper,
      weaponKillsSubmachinegun: stats.weaponKillsSubmachinegun,
      weaponKillsRelic: stats.weaponKillsRelic,
      weaponKillsSideArm: stats.weaponKillsSideArm,
      weaponKillsSword: stats.weaponKillsSword,
      weaponKillsAbility: stats.weaponKillsAbility,
      weaponKillsGrenade: stats.weaponKillsGrenade,
      weaponKillsGrenadeLauncher: stats.weaponKillsGrenadeLauncher,
      weaponKillsSuper: stats.weaponKillsSuper,
      weaponKillsMelee: stats.weaponKillsMelee
    };
    return weaponKills;
  }

  getMedals(stats: Stats): MedalStats {
    let medals: MedalStats = {
      medals_pvecomp_medal_invader_kill_four: stats.medals_pvecomp_medal_invader_kill_four,
      medals_pvecomp_medal_massacre: stats.medals_pvecomp_medal_massacre,
      medals_pvecomp_medal_no_escape: stats.medals_pvecomp_medal_no_escape,
      medals_pvecomp_medal_blockbuster: stats.medals_pvecomp_medal_blockbuster,
      medals_pvecomp_medal_tags_denied_15: stats.medals_pvecomp_medal_tags_denied_15,
      medals_pvecomp_medal_bank_kill: stats.medals_pvecomp_medal_bank_kill,
      medals_pvecomp_medal_denied: stats.medals_pvecomp_medal_denied,
      medals_pvecomp_medal_invasion_shutdown: stats.medals_pvecomp_medal_invasion_shutdown,
      medals_pvecomp_medal_locksmith: stats.medals_pvecomp_medal_locksmith,
      medals_pvecomp_medal_block_party: stats.medals_pvecomp_medal_block_party,
      medals_pvecomp_medal_never_say_die: stats.medals_pvecomp_medal_never_say_die,
      medals_pvecomp_medal_half_banked: stats.medals_pvecomp_medal_half_banked,
      medals_pvecomp_medal_everyone_invaded: stats.medals_pvecomp_medal_everyone_invaded,
      medals_pvecomp_medal_killmonger: stats.medals_pvecomp_medal_killmonger,
      medals_pvecomp_medal_thrillmonger: stats.medals_pvecomp_medal_thrillmonger,
      medals_pvecomp_medal_overkillmonger: stats.medals_pvecomp_medal_overkillmonger,
      medals_pvecomp_medal_first_to_block: stats.medals_pvecomp_medal_first_to_block,
      medals_pvecomp_medal_fast_fill: stats.medals_pvecomp_medal_fast_fill,
      medals_pvecomp_medal_big_game_hunter: stats.medals_pvecomp_medal_big_game_hunter,
      medals_pvecomp_medal_kill_after_invasion: stats.medals_pvecomp_medal_kill_after_invasion,
      medals_pvecomp_medal_revenge: stats.medals_pvecomp_medal_revenge,
      medals_pvecomp_medal_revenge_same_invasion: stats.medals_pvecomp_medal_revenge_same_invasion
    };
    return medals;
  }

  getPrecisionKillStats(stats: Stats): PrecisionWeaponKillStats {
    let precision: PrecisionWeaponKillStats = {
      weaponPrecisionKillsAutoRifle: stats.weaponPrecisionKillsAutoRifle,
      weaponPrecisionKillsBeamRifle: stats.weaponPrecisionKillsBeamRifle,
      weaponPrecisionKillsBow: stats.weaponPrecisionKillsBow,
      weaponPrecisionKillsFusionRifle: stats.weaponPrecisionKillsFusionRifle,
      weaponPrecisionKillsGrenade: stats.weaponPrecisionKillsGrenade,
      weaponPrecisionKillsHandCannon: stats.weaponPrecisionKillsHandCannon,
      weaponPrecisionKillsMelee: stats.weaponPrecisionKillsMelee,
      weaponPrecisionKillsPulseRifle: stats.weaponPrecisionKillsPulseRifle,
      weaponPrecisionKillsRocketLauncher: stats.weaponPrecisionKillsRocketLauncher,
      weaponPrecisionKillsScoutRifle: stats.weaponPrecisionKillsScoutRifle,
      weaponPrecisionKillsShotgun: stats.weaponPrecisionKillsShotgun,
      weaponPrecisionKillsSniper: stats.weaponPrecisionKillsSniper,
      weaponPrecisionKillsSubmachinegun: stats.weaponPrecisionKillsSubmachinegun,
      weaponPrecisionKillsSuper: stats.weaponPrecisionKillsSuper,
      weaponPrecisionKillsRelic: stats.weaponPrecisionKillsRelic,
      weaponPrecisionKillsSideArm: stats.weaponPrecisionKillsSideArm
    };
    return precision;
  }

  getWeaponAccuracy(stats: Stats): WeaponAccuracy {
    let accuracy: WeaponAccuracy = {
      weaponKillsPrecisionKillsAutoRifle: stats.weaponKillsPrecisionKillsAutoRifle,
      weaponKillsPrecisionKillsBeamRifle: stats.weaponKillsPrecisionKillsBeamRifle,
      weaponKillsPrecisionKillsBow: stats.weaponKillsPrecisionKillsBow,
      weaponKillsPrecisionKillsFusionRifle: stats.weaponKillsPrecisionKillsFusionRifle,
      weaponKillsPrecisionKillsGrenade: stats.weaponKillsPrecisionKillsGrenade,
      weaponKillsPrecisionKillsGrenadeLauncher: stats.weaponKillsPrecisionKillsGrenadeLauncher,
      weaponKillsPrecisionKillsHandCannon: stats.weaponKillsPrecisionKillsHandCannon,
      weaponKillsPrecisionKillsTraceRifle: stats.weaponKillsPrecisionKillsTraceRifle,
      weaponKillsPrecisionKillsMachineGun: stats.weaponKillsPrecisionKillsMachineGun,
      weaponKillsPrecisionKillsMelee: stats.weaponKillsPrecisionKillsMelee,
      weaponKillsPrecisionKillsPulseRifle: stats.weaponKillsPrecisionKillsPulseRifle,
      weaponKillsPrecisionKillsRocketLauncher: stats.weaponKillsPrecisionKillsRocketLauncher,
      weaponKillsPrecisionKillsScoutRifle: stats.weaponKillsPrecisionKillsScoutRifle,
      weaponKillsPrecisionKillsShotgun: stats.weaponKillsPrecisionKillsShotgun,
      weaponKillsPrecisionKillsSniper: stats.weaponKillsPrecisionKillsSniper,
      weaponKillsPrecisionKillsSubmachinegun: stats.weaponKillsPrecisionKillsSubmachinegun,
      weaponKillsPrecisionKillsSuper: stats.weaponKillsPrecisionKillsSuper,
      weaponKillsPrecisionKillsRelic: stats.weaponKillsPrecisionKillsRelic,
      weaponKillsPrecisionKillsSideArm: stats.weaponKillsPrecisionKillsSideArm
    };
    return accuracy;
  }

  getGambitStats(stats: Stats): GambitStats {
    let gambitStats: GambitStats = {
      invasions: stats.invasions,
      invasionKills: stats.invasionKills,
      invasionDeaths: stats.invasionDeaths,
      invaderKills: stats.invaderKills,
      invaderDeaths: stats.invaderDeaths,
      primevalKills: stats.primevalKills,
      blockerKills: stats.blockerKills,
      mobKills: stats.mobKills,
      highValueKills: stats.highValueKills,
      motesPickedUp: stats.motesPickedUp,
      motesDeposited: stats.motesDeposited,
      motesDenied: stats.motesDenied,
      motesLost: stats.motesLost,
      bankOverage: stats.bankOverage,
      smallBlockersSent: stats.smallBlockersSent,
      mediumBlockersSent: stats.mediumBlockersSent,
      largeBlockersSent: stats.largeBlockersSent,
      primevalDamage: stats.primevalDamage,
      primevalHealing: stats.primevalHealing
    };
    return gambitStats;
  }
}
