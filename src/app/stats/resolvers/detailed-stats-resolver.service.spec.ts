import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BungieApiService } from 'src/app/shared/services/bungie-api.service';


describe('DeatailedStatsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [BungieApiService]
  }));

  it('should be created', () => {
  });
});
