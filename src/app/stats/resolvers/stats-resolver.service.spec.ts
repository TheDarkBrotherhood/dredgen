import { TestBed } from '@angular/core/testing';

import { StatsResolverService } from './stats-resolver.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BungieApiService } from 'src/app/shared/services/bungie-api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CharacterEmblem } from 'src/app/shared/models/CharacterEmblem';
import { GambitRankIndex } from 'src/app/shared/models/GambitRankIndex';

describe('StatsResolverService', () => {
  let bungieApiService: BungieApiService;
  let service: StatsResolverService;
  let httpMock: HttpTestingController;
  const characterIds = ['2305843009271208511', '2305843009271208512', '2305843009271208513'];
  const characterEmblem: CharacterEmblem = {
    lightLevel: 734,
    level: 50,
    emblemPath: '/common/destiny2_content/icons/42f76f244536a06f324ddf337a2df41c.jpg',
    classType: 1,
    genderType: 0
  };
  const rankIndex: GambitRankIndex = {
    stepIndex: 13,
    resets: 0
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [BungieApiService, StatsResolverService]
    });
    bungieApiService = TestBed.get(BungieApiService);
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(StatsResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should recieve characterIds', (done) => {
    const destinyProfile = require('../../../mock-data/getDestinyProfileMock.json');

    service.getCharacterIds('4611686018450491554', '1').subscribe((ids: string[]) => {
      expect(ids).toEqual(characterIds);
    });

    const req =
      httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/1/Profile/4611686018450491554/?components=100');
    expect(req.request.method).toBe('GET');
    req.flush(destinyProfile);

    done();
  });

  it('should recieve characterEmblem data', (done) => {
    const destinyProfile = require('../../../mock-data/DestinyProfileComponent200Mock.json');

    service.getCharacterEmblem('4611686018450491554', '1', characterIds).subscribe((characterEmblems) => {
      expect(characterEmblems[0]).toEqual(characterEmblem);
    });

    const req =
      httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/1/Profile/4611686018450491554/?components=200');
    expect(req.request.method).toBe('GET');
    req.flush(destinyProfile);

    done();
  });

  it('should recieve gambit rank index', (done) => {
    const destinyProfile = require('../../../mock-data/DestinyProfileComponent202Mock.json');

    service.getGambitRank('4611686018450491554', '1', characterIds).subscribe((gambitRankIndex) => {
      expect(gambitRankIndex).toEqual(rankIndex);
    });

    const req =
      httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/1/Profile/4611686018450491554/?components=202');
    expect(req.request.method).toBe('GET');
    req.flush(destinyProfile);

    done();
  });
});
