import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from '@angular/router';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of, forkJoin } from 'rxjs';
import { DestinyProfile } from '../../shared/models/bungie-api/DestinyProfile';
import { CharacterEmblem } from '../../shared/models/CharacterEmblem';
import { GambitRankIndex } from '../../shared/models/GambitRankIndex';
import { DestinyCharacterProfile } from 'src/app/shared/models/bungie-api/DestinyCharacterProfile';

@Injectable({
  providedIn: 'root'
})
export class StatsResolverService implements Resolve<[string[], CharacterEmblem[], GambitRankIndex]> {

  constructor(private service: BungieApiService, private activatedRoute: ActivatedRoute, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<[string[], CharacterEmblem[], GambitRankIndex]> {
    let membershipType = route.paramMap.get('membershipType');
    let membershipId = route.paramMap.get('membershipId');

    return this.getCharacters(membershipId, membershipType).pipe(catchError(error => {
      this.router.navigate(['/profile/error']);
      return of(null);
    }));
  }

  getCharacters(membershipId: string, membershipType: string): Observable<[string[], CharacterEmblem[], GambitRankIndex]> {
    return this.getCharacterIds(membershipId, membershipType).pipe(
      mergeMap((destinyProfile) => forkJoin([
        of(destinyProfile.Response.profile.data.characterIds),
        this.getCharacterEmblem(membershipId, membershipType, destinyProfile),
        this.getGambitRank(membershipId, membershipType, destinyProfile.Response.profile.data.characterIds)
      ]))
    );
  }

  getCharacterIds(membershipId: string, membershipType: string) {
    return this.service.getProfile(membershipId, membershipType, {components: '100'}).pipe(
      map((destinyProfile: DestinyProfile) => {
        return destinyProfile;
      })
    );
  }

  getCharacterEmblem(membershipId: string, membershipType: string, destinyProfile: DestinyProfile): Observable<CharacterEmblem[]> {
    return this.service.getProfile(membershipId, membershipType, {components: '200'}).pipe(
      map((charactersData: DestinyCharacterProfile) => {
        let characterEmblem: CharacterEmblem[] = [];
        destinyProfile.Response.profile.data.characterIds.forEach((id, index) => {
          let characterId: string = id;
          let displayName: string = destinyProfile.Response.profile.data.userInfo.displayName;
          let lightLevel: number = charactersData.Response.characters.data[id].light;
          let level: number = charactersData.Response.characters.data[id].levelProgression.level;
          let emblemPath: string = charactersData.Response.characters.data[id].emblemBackgroundPath;
          let genderType: number = charactersData.Response.characters.data[id].genderType;
          let classType: number = charactersData.Response.characters.data[id].classType;
          characterEmblem.push({characterId, displayName, lightLevel, level, emblemPath, genderType, classType});
        });
        return characterEmblem;
      })
    );
  }

  getGambitRank(membershipId: string, membershipType: string, characterIds: string[]): Observable<GambitRankIndex> {
    return this.service.getProfile(membershipId, membershipType, {components: '202'}).pipe(
      map(gambitRank => {
        let step: number =
          gambitRank['Response']['characterProgressions']['data'][characterIds[0]]['progressions']['2772425241']['stepIndex'];
        let resetCounter: number =
          gambitRank['Response']['characterProgressions']['data'][characterIds[0]]['progressions']['2772425241']['currentResetCount'];
        let currentGambitRank: GambitRankIndex = {stepIndex: step, resets: resetCounter};
        return currentGambitRank;
      })
    );
  }
}
