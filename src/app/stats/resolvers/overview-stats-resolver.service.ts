import { Injectable } from '@angular/core';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { Observable, forkJoin } from 'rxjs';
import { HistoricalStats, HistoricalStatsResponse, Stats } from '../../shared/models/bungie-api/HistoricalStats';
import { map } from 'rxjs/operators';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { GambitStats } from '../../shared/models/DredgenStats';

@Injectable({
  providedIn: 'root'
})
export class OverviewStatsResolverService implements Resolve<(HistoricalStatsResponse|GambitStats)[]> {

  constructor(private service: BungieApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let membershipType = route.parent.paramMap.get('membershipType');
    let membershipId = route.parent.paramMap.get('membershipId');

    return this.getHistoricalStats(membershipId, membershipType);
  }

  getHistoricalStats(membershipId, membershipType): Observable<(HistoricalStatsResponse|GambitStats)[]> {
    return this.service.getHistoricalStats(membershipId, membershipType, '0', {modes: '64, 76'}).pipe(
      map((historicalStats: HistoricalStats) => {
        if (historicalStats.Response) {
          return [historicalStats.Response, this.getGambitStats(historicalStats.Response.allPvECompetitive.allTime)];
        } else {
          return null;
        }
      }
    ));
  }

  getGambitStats(stats: Stats): GambitStats {
    let gambitStats: GambitStats = {
      invasions: stats.invasions,
      invasionKills: stats.invasionKills,
      invasionDeaths: stats.invasionDeaths,
      invaderKills: stats.invaderKills,
      invaderDeaths: stats.invaderDeaths,
      primevalKills: stats.primevalKills,
      blockerKills: stats.blockerKills,
      mobKills: stats.mobKills,
      highValueKills: stats.highValueKills,
      motesPickedUp: stats.motesPickedUp,
      motesDeposited: stats.motesDeposited,
      motesDenied: stats.motesDenied,
      motesLost: stats.motesLost,
      bankOverage: stats.bankOverage,
      smallBlockersSent: stats.smallBlockersSent,
      mediumBlockersSent: stats.mediumBlockersSent,
      largeBlockersSent: stats.largeBlockersSent,
      primevalDamage: stats.primevalDamage,
      primevalHealing: stats.primevalHealing
    };
    return gambitStats;
  }
}
