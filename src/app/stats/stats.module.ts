import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsRoutingModule } from './stats-routing.module';
import { StatsComponent } from './stats.component';
import { OverviewComponent } from './overview/overview.component';
import { DetailedStatsComponent } from './detailed-stats/detailed-stats.component';
import { StatsErrorComponent } from './stats-error/stats-error.component';
import { GeneralStatisticsComponent } from './shared-components/general-statistics/general-statistics.component';
import { GambitRankComponent } from './shared-components/gambit-rank/gambit-rank.component';
import { GambitRolesStatsComponent } from './shared-components/gambit-roles-stats/gambit-roles-stats.component';
import { CharacterEmblemComponent } from './shared-components/character-emblem/character-emblem.component';
import { WeaponStatsComponent } from './shared-components/weapon-stats/weapon-stats.component';
import { HistoricalStatsComponent } from './shared-components/historical-stats/historical-stats.component';

@NgModule({
  declarations: [
    StatsComponent,
    OverviewComponent,
    DetailedStatsComponent,
    StatsErrorComponent,
    GambitRankComponent,
    GeneralStatisticsComponent,
    GambitRolesStatsComponent,
    CharacterEmblemComponent,
    WeaponStatsComponent,
    HistoricalStatsComponent
  ],
  imports: [
    CommonModule,
    StatsRoutingModule
  ]
})
export class StatsModule { }
