import { Component, OnInit, Input } from '@angular/core';
import { GambitRankIndex } from 'src/app/shared/models/GambitRankIndex';
import { ManifestService } from 'src/app/shared/services/manifest.service';
import { GambitRank } from 'src/app/shared/models/GambitRank';

@Component({
  selector: 'app-gambit-rank',
  templateUrl: './gambit-rank.component.html',
  styleUrls: ['./gambit-rank.component.scss']
})
export class GambitRankComponent implements OnInit {
  @Input() gambitRankIndex: GambitRankIndex;
  gambitRank: string;
  gambitRankIcon: string;

  constructor(private manifestService: ManifestService) { }

  ngOnInit() {
    this.manifestService.getGambitRank(this.gambitRankIndex.stepIndex)
    .then((gambit: GambitRank) => {
      this.gambitRank = gambit.rank;
      this.gambitRankIcon = gambit.icon;
    });
  }

}
