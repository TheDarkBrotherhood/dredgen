import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GambitRankComponent } from './gambit-rank.component';
import { BungieApiService } from 'src/app/shared/services/bungie-api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GambitRankComponent', () => {
  let component: GambitRankComponent;
  let fixture: ComponentFixture<GambitRankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ GambitRankComponent ],
      providers: [BungieApiService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GambitRankComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
