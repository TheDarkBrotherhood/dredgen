import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { GambitStats } from 'src/app/shared/models/DredgenStats';

@Component({
  selector: 'app-gambit-roles-stats',
  templateUrl: './gambit-roles-stats.component.html',
  styleUrls: ['./gambit-roles-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GambitRolesStatsComponent implements OnInit {
  @Input() stats: GambitStats;

  constructor() { }

  ngOnInit() {
  }

}
