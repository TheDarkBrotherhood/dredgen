import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GambitRolesStatsComponent } from './gambit-roles-stats.component';

describe('GambitRolesStatsComponent', () => {
  let component: GambitRolesStatsComponent;
  let fixture: ComponentFixture<GambitRolesStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GambitRolesStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GambitRolesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
