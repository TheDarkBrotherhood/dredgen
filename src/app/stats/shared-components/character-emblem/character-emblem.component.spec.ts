import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterEmblemComponent } from './character-emblem.component';
import { BungieApiService } from 'src/app/shared/services/bungie-api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CharacterEmblemComponent', () => {
  let component: CharacterEmblemComponent;
  let fixture: ComponentFixture<CharacterEmblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ CharacterEmblemComponent ],
      providers: [BungieApiService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterEmblemComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
