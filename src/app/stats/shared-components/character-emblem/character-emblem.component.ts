import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { CharacterEmblem } from 'src/app/shared/models/CharacterEmblem';
import { RaceType, ClassType } from 'src/app/shared/utility/enum';

@Component({
  selector: 'app-character-emblem',
  templateUrl: './character-emblem.component.html',
  styleUrls: ['./character-emblem.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CharacterEmblemComponent implements OnInit {
  @Input() characterEmblem: CharacterEmblem;

  constructor() { }

  ngOnInit() {}

  get raceType() {
    return RaceType;
  }

  get classType() {
    return ClassType;
  }

}
