import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalStatsComponent } from './historical-stats.component';

describe('HistoricalStatsComponent', () => {
  let component: HistoricalStatsComponent;
  let fixture: ComponentFixture<HistoricalStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
