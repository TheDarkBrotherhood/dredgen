import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Stats } from 'src/app/shared/models/bungie-api/HistoricalStats';

@Component({
  selector: 'app-historical-stats',
  templateUrl: './historical-stats.component.html',
  styleUrls: ['./historical-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoricalStatsComponent implements OnInit {

  @Input() stats: Stats;

  constructor() { }

  ngOnInit() {
  }

}
