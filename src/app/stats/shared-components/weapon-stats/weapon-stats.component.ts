import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { WeaponStats } from 'src/app/shared/models/DredgenStats';

@Component({
  selector: 'app-weapon-stats',
  templateUrl: './weapon-stats.component.html',
  styleUrls: ['./weapon-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeaponStatsComponent implements OnInit {
  @Input() weapon: WeaponStats;

  constructor() { }

  ngOnInit() {
  }

}
