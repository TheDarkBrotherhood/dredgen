import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Stats } from 'src/app/shared/models/bungie-api/HistoricalStats';

@Component({
  selector: 'app-general-statistics',
  templateUrl: './general-statistics.component.html',
  styleUrls: ['./general-statistics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GeneralStatisticsComponent implements OnInit {
  @Input() stats: Stats;

  constructor() { }

  ngOnInit() {
  }

}
