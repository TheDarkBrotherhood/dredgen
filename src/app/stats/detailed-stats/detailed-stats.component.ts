import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { CharacterEmblem } from 'src/app/shared/models/CharacterEmblem';
import { takeUntil } from 'rxjs/operators';
import { DestinyProfile } from 'src/app/shared/models/bungie-api/DestinyProfile';
import { HistoricalStats } from 'src/app/shared/models/bungie-api/HistoricalStats';
import { GambitRankIndex } from 'src/app/shared/models/GambitRankIndex';
import { DetailedStats, Gambit, Weapons } from 'src/app/shared/models/DredgenStats';

@Component({
  selector: 'app-detailed-stats',
  templateUrl: './detailed-stats.component.html',
  styleUrls: ['./detailed-stats.component.scss']
})
export class DetailedStatsComponent implements OnInit, OnDestroy {

  gambitRankIndex: GambitRankIndex;
  characterEmblems: CharacterEmblem[];
  options = [
    {name: 'Gambit', mode: 'gambit'},
    {name: 'Gambit Prime', mode: 'prime'},
    {name: 'Reckoning', mode: 'reckoning'}
  ];
  selected: string;
  characterId: string;
  historicalStats: HistoricalStats;
  weaponStats: Weapons;
  gambitStats: Gambit;
  private onDestroy = new Subject();

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(takeUntil(this.onDestroy)).subscribe(data => {
      this.characterId = data.get('characterId');
    });

    this.activatedRoute.queryParamMap.pipe(takeUntil(this.onDestroy)).subscribe(data => {
      this.selected = data.get('mode');
    });

    this.activatedRoute.parent.data.pipe(takeUntil(this.onDestroy))
      .subscribe((data: {characters: [DestinyProfile, CharacterEmblem[], GambitRankIndex]}) => {
        this.characterEmblems = data.characters[1];
        this.gambitRankIndex = data.characters[2];
    });

    this.activatedRoute.data.pipe(takeUntil(this.onDestroy)).subscribe((data: {detailedStats: DetailedStats}) => {
      this.historicalStats = data.detailedStats.historical;
      this.weaponStats = data.detailedStats.weapons;
      this.gambitStats = data.detailedStats.gambit;
    });
  }

  selectedMode(event: any) {
    this.selected = event.target.value;
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {mode: this.selected},
      queryParamsHandling: 'merge'
    });
  }

  /*
    Emits an empty value to the onDestroy subject upon component destroy.
    This unsubscribes everything to prevent memory leaks cause by long lived subscriptions.
  */
  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
