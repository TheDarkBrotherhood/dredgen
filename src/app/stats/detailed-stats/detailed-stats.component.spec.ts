import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedStatsComponent } from './detailed-stats.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { WeaponStatsComponent } from '../shared-components/weapon-stats/weapon-stats.component';
import { CharacterEmblemComponent } from '../shared-components/character-emblem/character-emblem.component';
import { GambitRankComponent } from '../shared-components/gambit-rank/gambit-rank.component';
import { GambitRolesStatsComponent } from '../shared-components/gambit-roles-stats/gambit-roles-stats.component';

describe('DetailedStatsComponent', () => {
  let component: DetailedStatsComponent;
  let fixture: ComponentFixture<DetailedStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [
        DetailedStatsComponent,
        CharacterEmblemComponent,
        GambitRankComponent,
        GambitRolesStatsComponent,
        WeaponStatsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedStatsComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
