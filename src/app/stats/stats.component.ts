import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { CharacterEmblem } from 'src/app/shared/models/CharacterEmblem';
import { takeUntil } from 'rxjs/operators';
import { GambitRankIndex } from 'src/app/shared/models/GambitRankIndex';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit, OnDestroy {
  characterIds: string[];
  private onDestroy = new Subject();

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.data.pipe(takeUntil(this.onDestroy))
      .subscribe((data: {characters: [string[], CharacterEmblem[], GambitRankIndex]}) => {
        this.characterIds = data.characters[0];
    });
  }

  redirectErrorStats() { // redirects to Error Stats component
    this.router.navigate(['/profile/error']);
  }

  redirectGeneralStats() { // redirects to General Stats component
    this.router.navigate(['overview'], {relativeTo: this.activatedRoute});
  }

  redirectDetailedStats() { // redirects to Detailed Stats component
    this.router.navigate(['detailed', this.characterIds[0]], {
      relativeTo: this.activatedRoute,
      queryParams: {mode: 'gambit'},
    });
  }

  /*
    Emits an empty value to the onDestroy subject upon component destroy.
    This unsubscribes everything to prevent memory leaks cause by long lived subscriptions.
  */
  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
