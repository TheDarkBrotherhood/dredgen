import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoricalStatsResponse, Stats } from 'src/app/shared/models/bungie-api/HistoricalStats';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { CharacterEmblem } from 'src/app/shared/models/CharacterEmblem';
import { GambitRankIndex } from 'src/app/shared/models/GambitRankIndex';
import { GambitStats } from 'src/app/shared/models/DredgenStats';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {
  gambitRankIndex: GambitRankIndex;
  characterEmblem: CharacterEmblem;
  pveComp: Stats;
  reckoning: Stats;
  gambitStats: GambitStats;
  private onDestroy = new Subject();

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.parent.data.pipe(takeUntil(this.onDestroy))
      .subscribe((data: {characters: [string[], CharacterEmblem[], GambitRankIndex]}) => {
        data.characters[1].forEach((id, index) => {
          if (this.characterEmblem == null) {
            this.characterEmblem = data.characters[1][index];
          }
          if (id.lightLevel > this.characterEmblem.lightLevel) {
            this.characterEmblem = data.characters[1][index];
          }
        });
        this.gambitRankIndex = data.characters[2];
    });

    this.activatedRoute.data.pipe(takeUntil(this.onDestroy)).subscribe((data: {overview: [HistoricalStatsResponse, GambitStats]}) => {
      this.gambitStats = data.overview[1];
      this.pveComp = data.overview[0].allPvECompetitive.allTime;
      this.reckoning = data.overview[0].enigma.allTime;
    });
  }

  /*
    Emits an empty value to the onDestroy subject upon component destroy.
    This unsubscribes everything to prevent memory leaks cause by long lived subscriptions.
  */
  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
