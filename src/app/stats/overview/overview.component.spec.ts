import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GambitRankComponent } from '../shared-components/gambit-rank/gambit-rank.component';
import { GambitRolesStatsComponent } from '../shared-components/gambit-roles-stats/gambit-roles-stats.component';
import { CharacterEmblemComponent } from '../shared-components/character-emblem/character-emblem.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GeneralStatisticsComponent } from '../shared-components/general-statistics/general-statistics.component';

describe('GeneralStatsComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        GambitRankComponent, GambitRolesStatsComponent,
        CharacterEmblemComponent,
        GeneralStatisticsComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
  });

  it('should create', () => {
  });
});
