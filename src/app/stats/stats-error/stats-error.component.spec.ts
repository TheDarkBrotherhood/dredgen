import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsErrorComponent } from './stats-error.component';

describe('StatsErrorComponent', () => {
  let component: StatsErrorComponent;
  let fixture: ComponentFixture<StatsErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
