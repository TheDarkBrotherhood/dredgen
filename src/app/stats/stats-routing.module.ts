import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatsComponent } from './stats.component';
import { StatsResolverService } from './resolvers/stats-resolver.service';
import { OverviewComponent } from './overview/overview.component';
import { OverviewStatsResolverService } from './resolvers/overview-stats-resolver.service';
import { DetailedStatsComponent } from './detailed-stats/detailed-stats.component';
import { DetailedStatsResolverService } from './resolvers/detailed-stats-resolver.service';
import { StatsErrorComponent } from './stats-error/stats-error.component';

const routes: Routes = [
  {
    path: ':membershipType/:membershipId/stats',
    component: StatsComponent,
    resolve : {
      characters: StatsResolverService
    },
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: OverviewComponent,
        resolve : {
          overview: OverviewStatsResolverService
        }
      },
      {
        path: 'detailed/:characterId',
        component: DetailedStatsComponent,
        resolve : {
          detailedStats: DetailedStatsResolverService
        }
      }
    ]
  },
  {
    path: 'error',
    component: StatsErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class StatsRoutingModule { }
