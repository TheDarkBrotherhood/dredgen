import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel } from '@angular/router';
import { Subject, Observable, isObservable } from 'rxjs';
import { BungieApiService } from './shared/services/bungie-api.service';
import { takeUntil } from 'rxjs/operators';
import { ManifestService } from './shared/services/manifest.service';
import { isString } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  loading: boolean;
  private onDestroy = new Subject();

  constructor(private router: Router, private destinyService: BungieApiService, private manifestService: ManifestService) { }

  ngOnInit() {
    this.routerEvents();
    this.checkManifest();
  }

  routerEvents(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.loading = true;
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        this.loading = false;
      }
    });
  }

  checkManifest() {
    this.manifestService.checkManifest()
    .then((data: Observable<any>) => {
      data.pipe(takeUntil(this.onDestroy)).subscribe((answer: string | Observable<string>) => {
        if (isObservable(answer)) {
          answer.pipe(takeUntil(this.onDestroy)).subscribe(help => console.log(help));
        }
        if (isString(answer)) {
          console.log(answer);
        }
      });
    });
  }

  /*
    Emits an empty value to the onDestroy subject upon component destroy.
    This unsubscribes everything to prevent memory leaks cause by long lived subscriptions.
  */
  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
