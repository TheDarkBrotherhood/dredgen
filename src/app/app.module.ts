import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { StateManagementService } from './shared/services/state-management.service';
import { AccountSearchModule } from './account-search/account-search.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NotificationsModule } from './notifications/notifications.module';

export function setUp(state: StateManagementService) {
  return () => state.getKeyOnStartUp();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    AccountSearchModule,
    NotificationsModule,
    AppRoutingModule,
  ],
  providers: [{provide: APP_INITIALIZER, useFactory: setUp, deps: [StateManagementService], multi: true}],
  bootstrap: [AppComponent]
})

export class AppModule { }
