import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServiceWorkerService } from '../../services/service-worker.service';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-update-notification',
  templateUrl: './update-notification.component.html',
  styleUrls: ['./update-notification.component.scss']
})
export class UpdateNotificationComponent implements OnInit, OnDestroy {
  updateAvailable: Observable<boolean>;
  private onDestroy = new Subject();

  constructor(private serviceWorker: ServiceWorkerService) {}

  ngOnInit() {
    this.serviceWorker.checkForUpdate();
    this.updateAvailable = this.serviceWorker.checkForAvailableUpdate();
  }

  close() {
    this.serviceWorker.activateClose();
  }

  update() {
    this.serviceWorker.activateUpdate();
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
