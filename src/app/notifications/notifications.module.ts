import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateNotificationComponent } from './components/update-notification/update-notification.component';



@NgModule({
  declarations: [UpdateNotificationComponent],
  imports: [
    CommonModule
  ],
  exports: [
    UpdateNotificationComponent
  ]
})
export class NotificationsModule { }
