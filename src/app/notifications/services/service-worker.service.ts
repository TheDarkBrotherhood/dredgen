import { Injectable, ApplicationRef } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { map, tap } from 'rxjs/operators';
import { interval, of, Subject, Observable, merge } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceWorkerService {
  private close = new Subject<boolean>();

  constructor(private swUpdate: SwUpdate) { }

  checkForAvailableUpdate() {
    return merge(
      of(false),
      this.swUpdate.available.pipe(map(() => true)),
      this.close.pipe(map(() => false))
    );
  }

  periodicallyCheckForUpdates() {
    return interval(6 * 60 * 60 * 1000).pipe(
      tap(() => this.swUpdate.checkForUpdate())
    );
  }

  activateClose() {
    this.close.next();
  }

  checkForUpdate() {
    this.swUpdate.checkForUpdate();
  }

  activateUpdate() {
    this.swUpdate.activateUpdate().then(() => {
      location.reload(true);
    });
  }

}
