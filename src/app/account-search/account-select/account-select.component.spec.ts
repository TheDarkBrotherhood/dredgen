import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSelectComponent } from './account-select.component';
import { RouterTestingModule } from '@angular/router/testing';
import { DebugElement } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DestinyPlayerResponse } from 'src/app/shared/models/bungie-api/SearchDestinyPlayer';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('AccountSelectComponent', () => {
  let component: AccountSelectComponent;
  let fixture: ComponentFixture<AccountSelectComponent>;
  let debug: DebugElement;
  let router: Router;
  const player: DestinyPlayerResponse[] = [
    {
      iconPath: '/img/theme/destiny/icons/icon_xbl.png',
      membershipType: 1,
      membershipId: '4611686018450491554',
      displayName: 'Theo Bassaw'
    }
  ];
  const routeData = { data: of({ accounts: player })};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ AccountSelectComponent ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: routeData
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSelectComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    debug = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve resolver data', () => {
    expect(component.accounts).toEqual(player);
  });

  it('should display button if accounts exist', () => {
    const buttonDe = debug.query(By.css('button[name="[XBL]"]'));
    expect(buttonDe).toBeDefined();
  });

  it('should not display button if accounts is null', () => {
    component.accounts = null;
    fixture.detectChanges();
    const buttonDe = debug.query(By.css('button[name="[XBL]"]'));
    expect(buttonDe).toBeNull();
  });

  it('should navigate on button click', () => {
    const navigateSpy = spyOn(router, 'navigate');
    const buttonDe = debug.query(By.css('button[name="[XBL]"]'));
    buttonDe.nativeElement.click();
    expect(navigateSpy).toHaveBeenCalledWith(['/profile/1/4611686018450491554/stats/']);
  });
});
