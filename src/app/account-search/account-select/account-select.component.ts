import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DestinyPlayerResponse } from 'src/app/shared/models/bungie-api/SearchDestinyPlayer';

@Component({
  selector: 'app-account-select',
  templateUrl: './account-select.component.html',
  styleUrls: ['./account-select.component.scss']
})
export class AccountSelectComponent implements OnInit, OnDestroy {
  accounts: DestinyPlayerResponse[];
  private onDestroy = new Subject();

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getPlayerResolverData();
  }

  /*
    Gets all accounts related to search entry from the account select resolver.
  */
  getPlayerResolverData() {
    this.activatedRoute.data.pipe(takeUntil(this.onDestroy)).subscribe((data: {accounts: DestinyPlayerResponse[]}) => {
      if (data.accounts == null || data.accounts.length < 1) {
        this.accounts = null;
      } else {
        this.accounts = data.accounts;
      }
    });
  }

  /*
    Navigates to Stats Module based on the account selected
  */
  selectedAccount(account: DestinyPlayerResponse) {
    this.router.navigate(['/profile/' + account.membershipType + '/' + account.membershipId + '/stats/']);
  }

  returnPlatformString(account: DestinyPlayerResponse) {
    if (account.membershipType === 1) {
      return '[XBL]';
    } else if (account.membershipType === 2) {
      return '[PSN]';
    } else {
      return '[PC]';
    }
  }

  /*
    Emits an empty value to the onDestroy subject upon component destroy.
    This unsubscribes everything to prevent memory leaks cause by long lived subscriptions.
  */
  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
