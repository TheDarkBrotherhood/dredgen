import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchBarComponent } from './search-bar/search-bar.component';
import { AccountSearchRoutingModule } from './account-search-routing.module';
import { AccountSelectComponent } from './account-select/account-select.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SearchBarComponent,
    AccountSelectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AccountSearchRoutingModule
  ],
  exports: [
    SearchBarComponent
  ]
})
export class AccountSearchModule { }
