import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountSelectComponent } from './account-select/account-select.component';
import { AccountSelectResolverService } from './resolvers/account-select-resolver.service';

const routes: Routes = [
  {
    path: 'account/:name',
    component: AccountSelectComponent,
    resolve: {
      accounts: AccountSelectResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AccountSearchRoutingModule { }
