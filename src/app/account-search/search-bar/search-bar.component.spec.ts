import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBarComponent } from './search-bar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { AccountSelectComponent } from '../account-select/account-select.component';

describe('SearchBarComponent', () => {
  let component: SearchBarComponent;
  let fixture: ComponentFixture<SearchBarComponent>;
  let debug: DebugElement;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      declarations: [SearchBarComponent, AccountSelectComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    debug = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test search', () => {
    const inputDe = debug.query(By.css('input[name="playerSearch"]'));
    const inputEl = inputDe.nativeElement;
    inputEl.value = 'soren42';
    inputEl.dispatchEvent(new Event('input'));
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.searchString).toMatch('soren42');
    });
  });

  it('should test enter key redirect', () => {
    const inputDe = debug.query(By.css('input[name="playerSearch"]'));
    const inputEl = inputDe.nativeElement;
    const navigateSpy = spyOn(router, 'navigate');

    inputEl.value = 'soren42';
    inputEl.dispatchEvent(new Event('input'));
    inputEl.dispatchEvent(new KeyboardEvent('keydown', {
      key: 'Enter'
    }));

    expect(navigateSpy).toHaveBeenCalledWith(['/account/soren42']);
  });

  it('should test button click redirect', () => {
    const inputDe = debug.query(By.css('input[name="playerSearch"]'));
    const buttonDe = debug.query(By.css('button[name="searchButton"]'));
    const inputEl = inputDe.nativeElement;
    const navigateSpy = spyOn(router, 'navigate');

    inputEl.value = 'soren42';
    inputEl.dispatchEvent(new Event('input'));
    buttonDe.nativeElement.click();

    expect(navigateSpy).toHaveBeenCalledWith(['/account/soren42']);
  });

});
