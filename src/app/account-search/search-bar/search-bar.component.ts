import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  searchString: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onKeyDown(event: any) { // on enter key navigate to player select
    if (event.key === 'Enter') {
      this.router.navigate(['/account/' + this.searchString.replace('#', '%23')]);
    }
  }

  onSumbitSearchPlayer() { // on button press navigate to player select
    this.router.navigate(['/account/' + this.searchString.replace('#', '%23')]);
  }

}
