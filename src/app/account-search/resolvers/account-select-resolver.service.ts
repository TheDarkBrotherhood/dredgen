import { Injectable } from '@angular/core';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { DestinyPlayerResponse } from '../../shared/models/bungie-api/SearchDestinyPlayer';

@Injectable({
  providedIn: 'root'
})
/*
  A resolver made for the Account Select component that returns a searchDestinyPlayer() response
*/
export class AccountSelectResolverService implements Resolve<DestinyPlayerResponse[]> {
  constructor(private destinyService: BungieApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let name = route.paramMap.get('name');
    return this.getDestinyPlayers('-1', name);
  }

  /*
    Returns all players based on membershipType and searchString. Using a membershipType of -1
    so that the search extends to all platforms
  */
  getDestinyPlayers(membershipType: string, searchString: string) {
    return this.destinyService.searchDestinyPlayer(membershipType, searchString).pipe(
      map(data => {
        return data.Response;
      })
    );
  }
}
