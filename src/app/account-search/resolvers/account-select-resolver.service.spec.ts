import { TestBed } from '@angular/core/testing';

import { AccountSelectResolverService } from './account-select-resolver.service';
import { BungieApiService } from '../../shared/services/bungie-api.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, convertToParamMap, RouterState } from '@angular/router';

describe('AccountSelectResolverService', () => {
  let httpMock: HttpTestingController;
  let resolver: AccountSelectResolverService;
  let route: ActivatedRoute;
  let routerState: RouterState;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        BungieApiService,
        {
          provide: ActivatedRoute,
          useValue: {snapshot: {paramMap: convertToParamMap({name: 'Theo Bassaw'})}}
        },
        {
          provide: RouterState,
          useValue: {snapshot: {url: '/account/Theo%20Bassaw'}}
        }
    ]
    });

    resolver = TestBed.get(AccountSelectResolverService);
    httpMock = TestBed.get(HttpTestingController);
    route = TestBed.get(ActivatedRoute);
    routerState = TestBed.get(RouterState);
  });

  afterEach(() => httpMock.verify());

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should return player search', (done) => {
    const playerSearch = require('../../../mock-data/SearchDestinyPlayerMock.json');

    resolver.resolve(route.snapshot, routerState.snapshot).subscribe(player => {
      expect(player[0].displayName).toEqual('Theo Bassaw');
    });

    const req = httpMock.expectOne('https://www.bungie.net/Platform/Destiny2/SearchDestinyPlayer/-1/Theo Bassaw/');
    expect(req.request.method).toBe('GET');
    req.flush(playerSearch);

    done();
  });
});
